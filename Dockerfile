FROM python:3.7.4-slim-buster

RUN pip install --upgrade pip
RUN apt-get update;\
	apt-get upgrade;\
        apt-get -y install gcc;

RUN mkdir -p /home/hdu/project
ADD . /home/hdu/project/
WORKDIR /home/hdu/project/

RUN pip install -r requirements.txt


ENV PYTHONUNBUFFERED=0
