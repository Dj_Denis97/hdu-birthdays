from django.contrib import admin

# Register your models here.
from birthday.models import Student, Employee, Administration, Structural


class StudentAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'structural_unit', 'birthday', 'end_date')
    search_fields = ('full_name', 'structural_unit__name', 'speciality')
    date_hierarchy = 'birthday'
    list_filter = ('structural_unit',)


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'department', 'position', 'birthday')
    search_fields = ('full_name', 'department', 'speciality', 'position')
    date_hierarchy = 'birthday'


class AdministrationAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'type', 'birthday')
    search_fields = ('full_name', 'position')
    date_hierarchy = 'birthday'
    list_filter = 'type',

admin.site.register(Student, StudentAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Administration, AdministrationAdmin)
admin.site.register(Structural)
