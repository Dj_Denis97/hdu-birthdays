# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
import csv
import pandas as pd
from birthday.models import Student, Structural
import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):
        bak_csv = pd.read_csv('бак.csv', delimiter=';')
        students = []
        for i in bak_csv.iterrows():
            student = {
                'full_name': i[1][0],
                'birthday': datetime.datetime.strptime(i[1][1], '%d.%m.%Y'),
                'sex': 'male' if i[1][2] == 'Чоловіча' else 'female',
                'end_date': datetime.datetime.strptime(i[1][3], '%d.%m.%Y'),
                'grade': 'bachelor' if i[1][5] == 'Бакалавр' else 'master',
                'speciality': i[1][6],
                'specialization': i[1][7] if type(i[1][7]) == str else ''
            }

            structural_init, created = Structural.objects.get_or_create(name=i[1][4])
            student['structural_unit'] = structural_init
            students.append(student)

        mag_csv = pd.read_csv('маг.csv', delimiter=';')

        for i in mag_csv.iterrows():
            student = {
                'full_name': i[1][0],
                'birthday': datetime.datetime.strptime(i[1][1], '%d.%m.%Y'),
                'sex': 'male' if i[1][2] == 'Чоловіча' else 'female',
                'end_date': datetime.datetime.strptime(i[1][3], '%d.%m.%Y'),
                'grade': 'bachelor' if i[1][5] == 'Бакалавр' else 'master',
                'speciality': i[1][6],
                'specialization': i[1][7] if type(i[1][7]) == str else ''
            }

            structural_init, created = Structural.objects.get_or_create(name=i[1][4])
            student['structural_unit'] = structural_init
            students.append(student)

        print('begin creation')
        for student in students:
            Student.objects.create(**student)
