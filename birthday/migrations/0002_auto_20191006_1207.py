# Generated by Django 2.2.6 on 2019-10-06 12:07

from django.db import migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('birthday', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='administration',
            name='photo',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='photos'),
        ),
        migrations.AddField(
            model_name='employee',
            name='photo',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='photos'),
        ),
        migrations.AddField(
            model_name='student',
            name='photo',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='photos'),
        ),
    ]
