from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

# Create your models here.

SEX_CHOICES = (
    ('male', 'Мужской'),
    ('female', 'Женский')
)


class Structural(models.Model):
    name = models.CharField('Название', max_length=250)

    def __str__(self):
        return self.name


class Student(models.Model):
    GRADE_CHOICES = (
        ('bachelor', 'Бакалавр'),
        ('master', 'Магистр')
    )

    full_name = models.CharField('Имя', max_length=200)
    birthday = models.DateField('Дата рождения')
    sex = models.CharField('Пол', max_length=20, choices=SEX_CHOICES, default='male')
    end_date = models.DateField('Дата окончания', blank=True, null=True)
    structural_unit = models.ForeignKey(Structural, verbose_name='Подразделение', on_delete=models.CASCADE)
    grade = models.CharField('Образовательный степень', max_length=50, choices=GRADE_CHOICES, default='bachelor')
    speciality = models.CharField('Специальность', max_length=250, blank=True, null=True)
    specialization = models.CharField('Специализация', max_length=250, blank=True, null=True)
    photo = ProcessedImageField(upload_to='photos',
                                processors=[ResizeToFill(200, 200)],
                                format='JPEG',
                                options={'quality': 60},
                                blank=True,
                                null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.full_name


class Employee(models.Model):
    full_name = models.CharField('Имя', max_length=200)
    birthday = models.DateField('Дата рождения')
    position = models.CharField('Должность', max_length=100, blank=True, null=True)
    department = models.CharField('Отдел', max_length=100, blank=True, null=True)
    photo = ProcessedImageField(upload_to='photos',
                                processors=[ResizeToFill(200, 200)],
                                format='JPEG',
                                options={'quality': 60},
                                blank=True,
                                null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.full_name


class Administration(models.Model):
    TYPE_CHOICES = (
        ('dean', 'Декан'),
        ('director', 'Керівник'),
        ('rector', 'Ректор')
    )

    full_name = models.CharField('Имя', max_length=200)
    birthday = models.DateField('Дата рождения')
    type = models.CharField('Тип', max_length=50, choices=TYPE_CHOICES)
    photo = ProcessedImageField(upload_to='photos',
                                processors=[ResizeToFill(200, 200)],
                                format='JPEG',
                                options={'quality': 60},
                                blank=True,
                                null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.full_name
