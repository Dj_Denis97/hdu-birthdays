"""hdu_birthdays URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from notification.views import AdminNotificationView, NotificationView
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import handler404
from notification.views import handler404 as view404

urlpatterns = [
    path('admin/', admin.site.urls),
    path('<date>/', NotificationView.as_view(), name='birthdays'),
    path('notify/<date>/', AdminNotificationView.as_view()),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
              static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = view404
