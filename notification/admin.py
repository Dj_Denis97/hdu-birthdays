from django.contrib import admin

# Register your models here.
from solo.admin import SingletonModelAdmin

from notification.models import Notification, StudentNotification, EmployeeNotification, AdministrationNotification, \
    Settings


class StudentNotificationAdmin(admin.TabularInline):
    model = StudentNotification
    extra = 0
    raw_id_fields = 'student',


class EmployeeNotificationAdmin(admin.TabularInline):
    model = EmployeeNotification
    extra = 0
    raw_id_fields = 'employee',


class AdministrationNotificationAdmin(admin.TabularInline):
    model = AdministrationNotification
    extra = 0
    raw_id_fields = 'administration',


class NotificationAdmin(admin.ModelAdmin):
    def get_students_count(item):
        return item.students.count()

    get_students_count.short_description = 'Кол-во студентов'

    def get_employees_count(item):
        return item.employees.count()

    get_employees_count.short_description = 'Кол-во сотрудников'

    def get_administrations_count(item):
        return item.administrations.count()

    get_administrations_count.short_description = 'Кол-во Администрации'

    inlines = StudentNotificationAdmin, EmployeeNotificationAdmin, AdministrationNotificationAdmin
    list_display = ('notification_date', get_students_count, get_employees_count, get_administrations_count)
    date_hierarchy = 'notification_date'
    raw_id_fields = ('students', 'employees', 'administrations')


admin.site.register(Notification, NotificationAdmin)
admin.site.register(Settings, SingletonModelAdmin)
