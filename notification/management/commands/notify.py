from django.core.management import BaseCommand
from django.utils.datetime_safe import datetime

from notification.models import Notification
from notification.utils import sent_notification, get_notification


class Command(BaseCommand):
    def handle(self, *args, **options):
        today = datetime.today()
        notification = get_notification(today)
        if notification and (notification.students.exists() or notification.employees.exists() or notification.administrations.exists()):
            sent_notification(notification)
