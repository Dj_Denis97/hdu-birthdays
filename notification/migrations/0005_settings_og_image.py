# Generated by Django 2.2.6 on 2019-11-04 09:27

from django.db import migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0004_auto_20191104_0820'),
    ]

    operations = [
        migrations.AddField(
            model_name='settings',
            name='og_image',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to='photos'),
        ),
    ]
