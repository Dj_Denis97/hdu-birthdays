from django.db import models

# Create your models here.
from imagekit.models import ProcessedImageField
from pilkit.processors import ResizeToFill
from solo.models import SingletonModel

from birthday.models import Student, Employee, Administration


class StudentNotification(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='notificationstudent_set')
    notification = models.ForeignKey('Notification', on_delete=models.CASCADE, related_name='studentnotification_set')
    sort = models.IntegerField('Порядок')

    def save(self, *args, **kwargs):
        if not self.sort:
            self.sort = StudentNotification.objects.filter(notification=self.notification).count() + 1
        super(StudentNotification, self).save(*args, **kwargs)


class EmployeeNotification(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='notificationemployee_set')
    notification = models.ForeignKey('Notification', on_delete=models.CASCADE, related_name='employeenotification_set')
    sort = models.IntegerField('Порядок')

    def save(self, *args, **kwargs):
        if not self.sort:
            self.sort = EmployeeNotification.objects.filter(notification=self.notification).count() + 1
        super(EmployeeNotification, self).save(*args, **kwargs)


class AdministrationNotification(models.Model):
    administration = models.ForeignKey(Administration, on_delete=models.CASCADE,
                                       related_name='notificationadministration_set')
    notification = models.ForeignKey('Notification', on_delete=models.CASCADE,
                                     related_name='administrationnotification_set')
    sort = models.IntegerField('Порядок')

    def save(self, *args, **kwargs):
        if not self.sort:
            self.sort = AdministrationNotification.objects.filter(notification=self.notification).count() + 1
        super(AdministrationNotification, self).save(*args, **kwargs)


class Notification(models.Model):
    notification_date = models.DateField('Дата уведомления')
    students = models.ManyToManyField(Student, through=StudentNotification)
    employees = models.ManyToManyField(Employee, through=EmployeeNotification)
    administrations = models.ManyToManyField(Administration, through=AdministrationNotification)

    created_at = models.DateTimeField(auto_now_add=True)


class Settings(SingletonModel):
    telegram_token = models.CharField(max_length=250)
    site_url = models.CharField(max_length=250)
    chat_id = models.CharField(max_length=250)

    no_student_photo = ProcessedImageField(upload_to='photos',
                                           processors=[ResizeToFill(200, 200)],
                                           format='JPEG',
                                           options={'quality': 60},
                                           blank=True,
                                           null=True)

    no_employee_photo = ProcessedImageField(upload_to='photos',
                                            processors=[ResizeToFill(200, 200)],
                                            format='JPEG',
                                            options={'quality': 60},
                                            blank=True,
                                            null=True)

    no_administration_photo = ProcessedImageField(upload_to='photos',
                                                  processors=[ResizeToFill(200, 200)],
                                                  format='JPEG',
                                                  options={'quality': 60},
                                                  blank=True,
                                                  null=True)

    og_image = ProcessedImageField(upload_to='photos',
                                   processors=[ResizeToFill(600, 600)],
                                   format='JPEG',
                                   options={'quality': 60},
                                   blank=True,
                                   null=True)

    notification_text = models.TextField('Текст уведомления', null=True, blank=True)
    notification_photo = models.ImageField(upload_to='photos', null=True, blank=True)
