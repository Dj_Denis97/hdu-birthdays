import requests
from django.urls import reverse
from notification.models import Settings
import telebot

from birthday.models import Student, Employee, Administration
from notification.models import Notification


def get_notification(date):
    instance = Notification.objects.filter(notification_date=date).last()
    if not instance:
        students = Student.objects.filter(birthday__day=date.day, birthday__month=date.month, end_date__gte=date)
        employees = Employee.objects.filter(birthday__day=date.day, birthday__month=date.month)
        administrations = Administration.objects.filter(birthday__day=date.day, birthday__month=date.month)
        instance = Notification(notification_date=date)
        instance.save()

        for student in students:
            instance.studentnotification_set.create(student=student)

        for employee in employees:
            instance.employeenotification_set.create(employee=employee)

        for administration in administrations:
            instance.administrationnotification_set.create(administration=administration)

    return instance


def sent_notification(notification):
    settings = Settings.objects.get()
    url = settings.site_url + reverse('birthdays', kwargs={'date': notification.notification_date})
    text = settings.notification_text
    photo = settings.notification_photo
    text += '\n' + url
    photo = requests.post('https://api.telegram.org/bot{api_key}/sendPhoto?chat_id=@{chat_id}&caption={caption}'.format(api_key=settings.telegram_token, chat_id=settings.chat_id, caption=text), files={'photo': photo})
    print(photo.json())
