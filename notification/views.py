from django.http import Http404
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.datetime_safe import datetime
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
# Create your views here.
from notification.utils import get_notification
from notification.models import Settings


def get_structurals(instance):
    structurals = {}

    for student in instance.students.all().order_by('notificationstudent_set__sort'):
        if student.structural_unit:
            if student.structural_unit.name in structurals.keys():
                structurals[student.structural_unit.name].append(student)
            else:
                structurals[student.structural_unit.name] = [student]
    return [{'name': x[0], 'students': x[1]} for x in structurals.items()]


class AdminNotificationView(TemplateView):
    template_name = 'admin_notification.html'

    def get(self, request, *args, **kwargs):
        settings = Settings.objects.last()
        user = request.user
        if not user.is_authenticated or not user.is_superuser:
            return handler404(request, *args, **kwargs)

        date = self.kwargs['date']
        instance = get_notification(datetime.strptime(date, '%Y-%m-%d').date())

        self.extra_context = {
            'employees': instance.employees.all().order_by('notificationemployee_set__sort'),
            'administrations': instance.administrations.all().order_by('notificationadministration_set__sort'),
            'structurals': get_structurals(instance),
            'no_student_photo': settings.no_student_photo.url if settings.no_student_photo else '',
            'no_employee_photo': settings.no_employee_photo.url if settings.no_employee_photo else '',
            'no_administration_photo': settings.no_administration_photo.url if settings.no_administration_photo else '',
            'og_image': settings.og_image.url if settings.og_image else ''
        }

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class NotificationView(TemplateView):
    template_name = 'admin_notification.html'

    def get(self, request, *args, **kwargs):
        settings = Settings.objects.last()
        date = self.kwargs['date']

        date = datetime.strptime(date, '%Y-%m-%d')
        today = datetime.today()

        if date > datetime.today() or (today - date).days > 1:
            return handler404(request, *args, **kwargs)

        instance = get_notification(date.date())

        self.extra_context = {
            'employees': instance.employees.all().order_by('notificationemployee_set__sort'),
            'administrations': instance.administrations.all().order_by('notificationadministration_set__sort'),
            'structurals': get_structurals(instance),
            'no_student_photo': settings.no_student_photo.url if settings.no_student_photo else '',
            'no_employee_photo': settings.no_employee_photo.url if settings.no_employee_photo else '',
            'no_administration_photo': settings.no_administration_photo.url if settings.no_administration_photo else '',
            'og_image': settings.og_image.url if settings.og_image else ''
        }

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


def handler404(request, template_name="404.html", *args, **kwargs):
    response = render_to_response("404.html")
    response.status_code = 404
    return response
